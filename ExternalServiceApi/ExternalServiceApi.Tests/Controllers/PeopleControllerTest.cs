﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExternalServiceApi.Infrastructure.Realization;
using ExternalServiceApi.Models;
using ExternalServiceApi.DTO;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace ExternalServiceApi.Tests.Controllers
{
    [TestClass]
    public class PeopleControllerTest
    {
        [TestMethod]
        public void GetPeopleTest()
        {
            PeopleRepository repo = new PeopleRepository();
            var result = repo.GetEnumerator();
            result.MoveNext();

            var resultObjList = new List<Person>();
            while (result.MoveNext())
                resultObjList.Add(result.Current);
            string str = JsonSerializer.Serialize<List<Person>>(resultObjList);

            Assert.IsNotNull(result);
            Assert.AreEqual("Ray", result.Current.Name);
        }

        [TestMethod]
        public void AddPersonTest()
        {
            var person = new Person { Name = "Jotabi", Age = 56 };
            PeopleRepository repo = new PeopleRepository();
            var result = repo.Add(person);
            Assert.IsNotNull(result);
            Assert.AreEqual("Guille", result.Name);
        }
    }
}
