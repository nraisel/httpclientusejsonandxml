﻿using ExternalServiceApi.Models;
using MongoDB.Driver;
using MongoRepository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ExternalServiceApi.Infrastructure.Realization
{
    public class PeopleRepository : IRepository<Person>
    {
        private MongoRepository<Person> context = new MongoRepository<Person>();
        public MongoCollection<Person> Collection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Person> entities)
        {
            throw new NotImplementedException();
        }

        public Person Add(Person entity)
        {
            return context.Add(entity);
        }

        public long Count()
        {
            throw new NotImplementedException();
        }

        public void Delete(Person entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<Person, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public bool Exists(Expression<Func<Person, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Person GetById(string id)
        {
            return context.GetById(id);
        }

        public IEnumerator<Person> GetEnumerator()
        {
            var result = context.GetEnumerator();
            var people = new List<Person>();
            while (result.MoveNext())
            {
                var obj = new Person
                {
                    Id = result.Current.Id,
                    Name = result.Current.Name,
                    Age = result.Current.Age
                };
                people.Add(obj);
            }
            return people.GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Person> entities)
        {
            throw new NotImplementedException();
        }

        public Person Update(Person entity)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return context.GetEnumerator();
        }
    }
}