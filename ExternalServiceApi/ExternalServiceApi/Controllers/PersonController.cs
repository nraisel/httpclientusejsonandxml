﻿using ExternalServiceApi.DTO;
using ExternalServiceApi.Infrastructure.Realization;
using ExternalServiceApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ExternalServiceApi.Controllers
{
    public class PersonController : ApiController
    {
        // GET: Person
        public IHttpActionResult Get()
        {
            var repo = new PeopleRepository();
            var result = repo.GetEnumerator();
            if (result == null)
                return NotFound();
            else
            {
                var resultObjList = new PeopleList();
                while (result.MoveNext())
                    resultObjList.People.Add(result.Current);
                //string obj = JsonSerializer.Serialize<List<Person>>(resultObjList.People);
                return Ok(resultObjList.People.ToList());
            }
                
        }

        public IHttpActionResult GetById(string id)
        {
            var repo = new PeopleRepository();
            var result = repo.GetById(id);
            if (result == null)
                return NotFound();
            else
            {
                return Ok(new Person { Id = result.Id, Name = result.Name, Age = result.Age });
            }

        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult Add(Person person)
        {
            var repo = new PeopleRepository();
            var result = repo.Add(person);
            if (result == null)
                return BadRequest();
            else
                return Ok(result);
        }
    }
}