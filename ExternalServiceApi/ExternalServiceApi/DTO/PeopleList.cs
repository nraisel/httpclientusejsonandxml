﻿using ExternalServiceApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExternalServiceApi.DTO
{
    public class PeopleList
    {
        public IList<Person> People { get; set; }
        public PeopleList()
        {
            People = new List<Person>();
        }
    }
}